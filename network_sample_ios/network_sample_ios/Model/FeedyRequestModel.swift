//
//  Model.swift
//  network_sample_ios
//
//  Created by Quang Luu on 11/14/17.
//  Copyright © 2017 OFFICIENCE SARL. All rights reserved.
//

import Foundation

struct FeedyRequestModel {
    var latestNews: String
    
    init(latestNews:String) {
        self.latestNews = latestNews
    }
}
