
//
//  FeedyModel.swift
//  network_sample_ios
//
//  Created by Quang Luu on 11/14/17.
//  Copyright © 2017 OFFICIENCE SARL. All rights reserved.
//

import Foundation

struct FeedyModel{
    
    var content : String!
    var descriptionField : String!
    var idCa : String!
    var idPost : String!
    var image : String!
    var nameCa : String!
    var time : String!
    var title : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        content = dictionary["content"] as? String
        descriptionField = dictionary["description"] as? String
        idCa = dictionary["id_ca"] as? String
        idPost = dictionary["id_post"] as? String
        image = dictionary["image"] as? String
        nameCa = dictionary["name_ca"] as? String
        time = dictionary["time"] as? String
        title = dictionary["title"] as? String
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if content != nil{
            dictionary["content"] = content
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if idCa != nil{
            dictionary["id_ca"] = idCa
        }
        if idPost != nil{
            dictionary["id_post"] = idPost
        }
        if image != nil{
            dictionary["image"] = image
        }
        if nameCa != nil{
            dictionary["name_ca"] = nameCa
        }
        if time != nil{
            dictionary["time"] = time
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }
    
}
