//
//  ViewController.swift
//  network_sample_ios
//
//  Created by Quang Luu on 11/14/17.
//  Copyright © 2017 OFFICIENCE SARL. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
   
    private let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil)
    private let cellIdentifier = FeedyTableViewCell.identity
    private var viewModel: FeedyViewModel!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set up
        setupViewModel()
        setupTableView()
        setupTableViewBinding()
    }
    
    //MARK: - set up methods
    private func setupViewModel() {
        self.viewModel = FeedyViewModel()
    }
    
    private func setupTableViewBinding() {
        viewModel.dataSource
            .asObservable()
            .bind(to: tableView
                .rx.items(cellIdentifier: cellIdentifier,
                                         cellType: FeedyTableViewCell.self)) {  row, element, cell in
                                            cell.feedyImage.downloadedFrom(link: element.image)
                                            cell.categoryLabel.text = element.time
                                            cell.descriptionLabel.text = element.descriptionField
                                            cell.titleLabel.text = element.title
                                            
                                            
                                            
            }
            .disposed(by: disposeBag)
    }
    
    private func setupTableView() {
        //This is necessary since the UITableViewController automatically set his tableview delegate and dataSource to self
        tableView.delegate = nil
        tableView.dataSource = nil
        
        tableView.tableFooterView = UIView() //Prevent empty rows
        
        tableView.rowHeight = 130.0
        
        tableView.register(UINib(nibName: FeedyTableViewCell.identity, bundle: nil), forCellReuseIdentifier: FeedyTableViewCell.identity)
    }
    
    
}

