//
//  Viewmodel.swift
//  network_sample_ios
//
//  Created by Huy Lam on 11/15/17.
//  Copyright © 2017 OFFICIENCE SARL. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class FeedyViewModel {
    
    // MARK: Private properties
    private let disposeBag = DisposeBag()
    
    // MARK: Outputs
    public var dataSource: Variable<[FeedyModel]> = Variable([])
    
    init() {
        getFeedyList()
    }
    
    private func getFeedyList() {
        FeedyService(token: "").getFeedyList(parameter: FeedyRequestModel(latestNews: "25"))
            .subscribe(onNext: { feedyList in
                self.dataSource.value = feedyList
            }, onError: { error in
                debugPrint(error)
            }, onCompleted: {
                debugPrint("onCompleted")
            }, onDisposed: {
                debugPrint("onDisposed")
            }).disposed(by: disposeBag)
    }
}
