//
//  ServiceProtocol.swift
//  network_sample_ios
//
//  Created by Quang Luu on 11/14/17.
//  Copyright © 2017 OFFICIENCE SARL. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

protocol ServiceProtocol {
    var token: String {get set}
}

extension ServiceProtocol {

}
