//
//  WSWorker.swift
//  network_sample_ios
//
//  Created by Quang Luu on 11/14/17.
//  Copyright © 2017 OFFICIENCE SARL. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

typealias JSONDictionary = [String: Any]

func apiError(_ error: String) -> NSError {
    return NSError(domain: "NetworkSampleAPI", code: -1, userInfo: [NSLocalizedDescriptionKey: error])
}

func exampleError(_ error: String, location: String = "\(#file):\(#line)") -> NSError {
    return NSError(domain: "ExampleError", code: -1, userInfo: [NSLocalizedDescriptionKey: "\(location): \(error)"])
}

class WSWorker {
    
    lazy var sessionManager = SessionManager()
    private let route: URLRequestConvertible
    private var request: DataRequest!
    
    init(accessToken: String, route: URLRequestConvertible) {
        self.route = route
        self.sessionManager.adapter = AccessTokenAdapter(accessToken: accessToken, baseURL:route.urlRequest?.url?.absoluteString ?? "")
    }
    
    func performObjectRequest() -> Observable<JSONDictionary> {
        return Observable.create({observer -> Disposable in
            self.request = self.sessionManager.request(self.route).responseJSON { response in
                debugPrint(response)
                switch response.result {
                case .success:
                    if let jsonDict = response.result.value as? JSONDictionary {
                        observer.onNext(jsonDict)
                    }
                    observer.onCompleted()
                case .failure(let error):
                    debugPrint(error)
                    //observer.onError(error)
                    observer.onError(apiError(error.localizedDescription))
                    //observer.onError(APIError.serverError)
                }
            }
            return Disposables.create {
                self.request.cancel()
            }
        })
    }
    
    func performObjectRequest() -> Observable<[JSONDictionary]> {
        return Observable.create({observer -> Disposable in
            self.request = self.sessionManager.request(self.route).responseJSON { response in
                debugPrint(response)
                switch response.result {
                case .success:
                    if let jsonDict = response.result.value as? [JSONDictionary] {
                        observer.onNext(jsonDict)
                    }
                    observer.onCompleted()
                case .failure(let error):
                    debugPrint(error)
                    //observer.onError(error)
                    observer.onError(apiError(error.localizedDescription))
                    //observer.onError(APIError.serverError)
                }
            }
            return Disposables.create {
                self.request.cancel()
            }
        })
    }
    
    
    //    func performObjectRequest(responseHandler: @escaping (JSONDictionary?, APIError?) -> Void) -> DataRequest {
    //        let request = sessionManager.request(route).responseJSON { response in
    //            debugPrint(response)
    //            switch response.result {
    //            case .success:
    //                responseHandler(response.result.value as? JSONDictionary, nil)
    //            case .failure(let error):
    //                debugPrint(error)
    //                responseHandler(nil, error as? APIError)
    //            }
    //        }
    //        return request
    //    }
    //
    //    func performCollectionRequest(responseHandler: @escaping (JSONDictionary?, APIError?) -> Void) -> DataRequest{
    //        let request = sessionManager.request(route).responseJSON { response in
    //            debugPrint(response)
    //            switch response.result {
    //            case .success:
    //                responseHandler(response.result.value as? JSONDictionary, nil)
    //            case .failure(let error):
    //                debugPrint(error)
    //                responseHandler(nil, error as? APIError)
    //            }
    //        }
    //        return request
    //    }
}
