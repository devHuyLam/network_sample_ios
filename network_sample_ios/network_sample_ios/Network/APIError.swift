//
//  ServiceError.swift
//  network_sample_ios
//
//  Created by Quang Luu on 11/15/17.
//  Copyright © 2017 OFFICIENCE SARL. All rights reserved.
//

import Foundation

import Foundation

public enum APIError: Error {
    case unauthorized
    case serverError
}

// MARK: - Error Descriptions

extension APIError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .unauthorized:
            return "Unauthorized."
        case .serverError:
            return "Server."
        }
    }
}
