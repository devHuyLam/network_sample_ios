//
//  AccessTokenAdapter.swift
//  network_sample_ios
//
//  Created by Quang Luu on 11/14/17.
//  Copyright © 2017 OFFICIENCE SARL. All rights reserved.
//

import Alamofire

class AccessTokenAdapter: RequestAdapter {
    private let accessToken: String
    private let baseURL: String
    
    init(accessToken: String, baseURL: String) {
        self.accessToken = accessToken
        self.baseURL = baseURL
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        
        if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(baseURL) {
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            urlRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
        }
        
        return urlRequest
    }
}
