//
//  Router.swift
//  network_sample_ios
//
//  Created by Quang Luu on 11/14/17.
//  Copyright © 2017 OFFICIENCE SARL. All rights reserved.
//

import Alamofire

enum FeedyRouter: URLRequestConvertible {
    case getFeed(lastestNews: Parameters)
    
    static let baseURLString = "http://dev.2dev4u.com"
    
    var method: HTTPMethod {
        switch self {
        case .getFeed:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getFeed:
            return "/news/api.php"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try FeedyRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getFeed(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        default:
            break
        }
        
        return urlRequest
    }
}
