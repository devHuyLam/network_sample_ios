//
//  FeedyService.swift
//  network_sample_ios
//
//  Created by Quang Luu on 11/14/17.
//  Copyright © 2017 OFFICIENCE SARL. All rights reserved.
//

import Foundation
import RxSwift

class FeedyService {
    var token: String
    
    init(token: String) {
        self.token = token
    }
    
    func getFeedyList(parameter: FeedyRequestModel) -> Observable<[FeedyModel]> {
        // Get data from feeddy
        // And put to Router
        // as a parameter
        let params = ["latest_news": parameter.latestNews]
        let route = FeedyRouter.getFeed(lastestNews: params)
        return WSWorker(accessToken: token, route: route).performObjectRequest().map {json in
            return json.map{FeedyModel(fromDictionary: $0)}
        }
    }
}
